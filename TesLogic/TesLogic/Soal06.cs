﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesLogic
{
    class Soal06
    {
        public Soal06()
        {
            Console.WriteLine("=== Naik Turun / NT ===");
            Console.Write("Masukan Input: ");
            string input = Console.ReadLine();
            string nt = "NT";
            int changed = 0;

            //int mdpl = 0;
            //int gunung = 0;
            //int lembah = 0;
            //int perjalanan = 0;

            //for (int i = 0; i < length; i++)
            //{
            //    if (true)
            //    {

            //    }
            //}

            if (input.Length % nt.Length == 0)
            {
                int loop = input.Length / nt.Length;
                for (int i = 0; i < loop; i++)
                {
                    if (nt == input.Substring(i * nt.Length, nt.Length))
                    {
                        changed++;
                    }
                }
                Console.WriteLine("Changed letter {0}", changed);
            }
            else
            {
                Console.WriteLine("Input Salah");
            }
        }
    }
}
