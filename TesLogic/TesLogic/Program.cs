﻿using System;

namespace TesLogic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            new Program();
        }
        public Program()
        {
            bool selesai = false;
            while (!selesai)
            {
                Console.WriteLine("=======  Tes Evaluasi 1  =======");
                Console.WriteLine("  Soal 1. Eskrim");
                Console.WriteLine("  Soal 2. Sort Huruf");
                Console.WriteLine("  Soal 3. Fibonachi");
                Console.WriteLine("  Soal 4. Kelipatan 3");
                Console.WriteLine("  Soal 5. Persegi");
                Console.WriteLine("  Soal 6. Naik Turun");
                Console.WriteLine("  Soal 7. Bobot Pada Huruf Alfabet");
                Console.WriteLine("  Soal 8. Libur Bersama");
                Console.WriteLine("  Soal 9. Cupcake");
                Console.WriteLine("  Soal 10. Jumlah Panjang Deret");
                Console.WriteLine("=======================");
                Console.WriteLine();
                Console.Write("Pilih soal : ");
                int soal = int.Parse(Console.ReadLine());
                switch (soal)
                {
                    case 1:
                        Soal01 soal01 = new Soal01();
                        break;
                    case 2:
                        Soal02 soal02 = new Soal02();
                        break;
                    case 3:
                        Soal03 soal03 = new Soal03();
                        break;
                    case 4:
                        Soal04 soal04 = new Soal04();
                        break;
                    case 5:
                        Soal05 soal05 = new Soal05();
                        break;
                    case 6:
                        Soal06 soal06 = new Soal06();
                        break;
                    case 7:
                        Soal07 soal07 = new Soal07();
                        break;
                    case 8:
                        Soal08 soal08 = new Soal08();
                        break;
                    case 9:
                        Soal09 soal09 = new Soal09();
                        break;
                    case 10:
                        Soal10 soal10 = new Soal10();
                        break;
                    default:
                        Console.WriteLine("Tidak ada dalam pilihan");
                        break;
                }
                Console.WriteLine();
                Console.Write("Apakah Ingin Keluar? [Y/T] : ");
                selesai = Console.ReadLine().ToUpper() != "T";
                Console.Clear();
            }
        }
    }
}
