﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesLogic
{
    class Soal03
    {
        public Soal03()
        {
            Console.WriteLine("==== Fibonachi Angka Genap ====");
            Console.Write("Input x : ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int a = 0;
            int b = 1;
            int result = 0;


            for (int i = 0; i < x; i++)
            {
                a = b;
                b = result;
                result = a + b;

                if (result % 2 == 0)
                {
                    Console.Write("{0}", result);
                }
            }

        }
    }
}
